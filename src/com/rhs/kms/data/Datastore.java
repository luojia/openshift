package com.rhs.kms.data;

import java.util.Properties;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

public enum Datastore {
	INSTANCE;
	
	private final PersistenceManagerFactory pmf;
	
	Datastore() {
		Properties properties = new Properties();
//    	properties.setProperty("javax.jdo.PersistenceManagerFactoryClass", "{my_implementation_pmf_class}");
//    	properties.setProperty("javax.jdo.option.ConnectionDriverName","com.mysql.jdbc.Driver");
    	properties.setProperty("javax.jdo.option.ConnectionURL","mongodb:/openShift");
//    	properties.setProperty("javax.jdo.option.ConnectionUserName","login");
//    	properties.setProperty("javax.jdo.option.ConnectionPassword","password");
    	pmf = JDOHelper.getPersistenceManagerFactory(properties); 
	}
	
	public PersistenceManager getPersistenceManager() {
		return pmf.getPersistenceManager();
	}
}
