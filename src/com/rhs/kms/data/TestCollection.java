package com.rhs.kms.data;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class TestCollection {
	@PrimaryKey
	private String _id;

	@Persistent
	private String timestamp;

	@Override
	public String toString() {
		return "TestCollection [" + (_id != null ? "_id=" + _id + ", " : "")
				+ (timestamp != null ? "timestamp=" + timestamp + ", " : "")
				+ "]";
	}

	public TestCollection(String _id, String timestamp) {
		super();
		this._id = _id;
		this.timestamp = timestamp;
	}

	public String getTimeStamp() {
		return timestamp;
	}

	public void setTimeStamp(String timestamp) {
		this.timestamp = timestamp;
	}
}