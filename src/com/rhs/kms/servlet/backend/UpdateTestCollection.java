package com.rhs.kms.servlet.backend;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rhs.kms.data.Datastore;
import com.rhs.kms.data.TestCollection;

/**
 * Servlet implementation class UpdateTestCollection
 */
@WebServlet("/UpdateTestCollection")
public class UpdateTestCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateTestCollection() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		PersistenceManager pm = Datastore.INSTANCE.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		
		try {
			out.println("Updating testCollection...");
			
			List<TestCollection> testCollectionItems = new ArrayList<TestCollection>();
			for (int i=0; i<3; i++) {
				String id = "haha"+i;
				Date dNow = new Date( );
				SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
				String timestamp = ft.format(dNow);
				TestCollection testCollectionItem = new TestCollection(id, timestamp);
				testCollectionItems.add(testCollectionItem);
				tx.begin();
				out.println(testCollectionItem);
				pm.makePersistent(testCollectionItem);
				tx.commit();
			}
			out.println("Update testCollection success.");
		} catch (JDOException e) {
			if (tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
			out.println("Update testCollection fail.");
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
			out.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
