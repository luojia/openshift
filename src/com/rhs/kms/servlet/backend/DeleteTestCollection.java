package com.rhs.kms.servlet.backend;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rhs.kms.data.Datastore;
import com.rhs.kms.data.TestCollection;

/**
 * Servlet implementation class DeleteTestCollection
 */
@WebServlet("/DeleteTestCollection")
public class DeleteTestCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteTestCollection() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		PersistenceManager pm = Datastore.INSTANCE.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		
		try {
			out.println("Deleting testCollection...");
			
			Query q = pm.newQuery(TestCollection.class);
			tx.begin();
			q.deletePersistentAll();
			tx.commit();
			out.println("Deleting testCollection success.");
		} catch (JDOException e) {
			if (tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
			out.println("Deleting testCollection fail.");
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
			out.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
